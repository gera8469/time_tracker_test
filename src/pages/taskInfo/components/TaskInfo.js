import React from 'react';
import {
  Link,
} from 'react-router-dom';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import {TaskDataManager} from '../../../models';
import {TaskChart} from '../../../components';
import {RaisedButton} from 'material-ui';
import '../styles/stylesheet.css';

export default function TaskInfo({match}) {
  const task = TaskDataManager.getTaskById(match.params.taskId);
  return (
      <div className={'taskInfo'}>
        <RaisedButton
            className={'button'}
            containerElement={<Link to="/"/>}>
          HOME
        </RaisedButton>
        <h1 className={'mainHeader'}>Task info</h1>
        <Table className={'taskTable'}>
          <TableHeader
              displaySelectAll={false}
              adjustForCheckbox={false}
          >
            <TableRow>
              <TableHeaderColumn
                  className={'taskTable__cell'}>Name of task</TableHeaderColumn>
              <TableHeaderColumn
                  className={'taskTable__cell'}>Time start</TableHeaderColumn>
              <TableHeaderColumn
                  className={'taskTable__cell'}>Time end</TableHeaderColumn>
              <TableHeaderColumn
                  className={'taskTable__cell'}>Time spend</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody

              displayRowCheckbox={false}
              showRowHover={true}
          >
            <TableRow key={task.id} className={`taskTable__row`}>
              <TableRowColumn
                  className={'taskTable__cell taskTable__cell_bodyCell'}>
                {task.taskName}
              </TableRowColumn>
              <TableRowColumn
                  className={'taskTable__cell taskTable__cell_bodyCell'}>
                {task.startTime}
              </TableRowColumn>
              <TableRowColumn
                  className={'taskTable__cell taskTable__cell_bodyCell'}>
                {task.endTime}
              </TableRowColumn>
              <TableRowColumn
                  className={'taskTable__cell taskTable__cell_bodyCell'}>
                {task.timeCompletedTask}
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
        <TaskChart
            taskList={[task]}
        />
      </div>
  );
}
