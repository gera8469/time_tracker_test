import React, {Component} from 'react';
import {TaskChart, TaskList, TaskManager} from '../../../components';
import {Task} from '../../../models';
import '../styles/stylesheet.css';

export class MainPage extends Component {
  filterTasksByCurrentDate = () => {
    return this.props.timeTracker.taskList.filter(task => {
        return task.isTaskActual();
    });
  };

  render() {
    const {
      timeTracker: {activeTask, taskList},
      removeTask,
      startNewTask,
      stopNewTask,
    } = this.props;
    return (
        <div>
          <TaskManager
              onClickStart={startNewTask}
              onClickStop={stopNewTask}
              activeTask={activeTask ? activeTask : new Task()}
              isActive={!!activeTask}
          />
          <TaskList
              taskObjList={taskList}
              removeTaskHandler={removeTask}
          />
          <TaskChart
              taskList={this.filterTasksByCurrentDate()}
          />
        </div>
    );
  }
}
