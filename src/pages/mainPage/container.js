import {MainPage as MainPageComponent} from './components/MainPage';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {actions} from '../../redux';

const MainPage = withRouter(connect(
    state => state,
    dispatch => (actions)
)(MainPageComponent));

export default MainPage;
