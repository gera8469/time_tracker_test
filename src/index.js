import React from 'react';
import ReactDOM from 'react-dom';
import {MainPage} from './pages/mainPage';
import {Provider} from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import {createStore} from 'redux';
import {TaskInfo} from './pages/taskInfo';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {reducers} from './redux';

const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__
    && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
    <MuiThemeProvider>
      <Provider store={store}>
        <Router>
          <div>
            <Route exact path="/" component={MainPage}/>
            <Route path="/info/:taskId" component={TaskInfo}/>
          </div>
        </Router>
      </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);
