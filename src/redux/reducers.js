import update from 'react-addons-update';
import {TaskDataManager} from '../models/TaskDataManager';
import {ADD_TASK, REMOVE_TASK, STOP_ACTIVE_TASK} from './actions';

const initialState = {
  taskList: TaskDataManager.getTaskObjList(),
  activeTask: TaskDataManager.getActiveTaskObj(),
};

export default function timeTracker(state = initialState, action) {
  switch (action.type) {
    case ADD_TASK:
      return update(state, {
        activeTask: {$set: action.activeTask},
      });
    case STOP_ACTIVE_TASK:
      return update(state, {
        taskList: {$push: [action.activeTask]},
        activeTask: {$set: null},
      });
    case REMOVE_TASK:
      return update(state, {
        taskList: {$set: action.taskList},
      });
    default:
      return state;
  }
}
