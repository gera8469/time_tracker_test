import {TaskDataManager} from '../models';
import {Task} from '../models';

const ADD_TASK = `ADD_TASK`;
const STOP_ACTIVE_TASK = `STOP_ACTIVE_TASK`;
const REMOVE_TASK = `REMOVE_TASK`;

const actions = dispatch => ({
  startNewTask: (taskName) => {
    const activeTask = new Task(taskName, Date.now());
    TaskDataManager.setActiveTask(activeTask);
    dispatch({
      type: ADD_TASK,
      activeTask,
    });
  },
  stopNewTask: (activeTask) => {
    activeTask.endTimeStamp = Date.now();
    TaskDataManager.clearActiveTask();
    TaskDataManager.addTask(activeTask);
    dispatch({
      type: STOP_ACTIVE_TASK,
      activeTask,
    });
  },
  removeTask: (taskId) => {
    const taskList = TaskDataManager.removeTask(taskId);
    dispatch({
      type: REMOVE_TASK,
      taskList: taskList,
    });
  },
});

export {
  actions,
  ADD_TASK,
  STOP_ACTIVE_TASK,
  REMOVE_TASK,
};
