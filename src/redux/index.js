import {combineReducers} from 'redux';
import timeTracker from './reducers';
export {actions} from './actions';
export const reducers = combineReducers({
  timeTracker,
});
