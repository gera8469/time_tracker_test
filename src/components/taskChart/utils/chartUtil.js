import moment from 'moment';
import {
  CURRENT_DATE_FILTER,
  DATE_FORMAT,
} from '../../../models/TaskDataManager';

export default function generateChartData(taskList = []) {
  const chartData
      = new Array(24).fill(0).map((minutes, name) => ({name, minutes}));

  taskList.forEach(task => {
    const startDate = moment(task.startTimeStamp);
    const endDate = moment(task.endTimeStamp);
    const {startHour, endHour}
        = calculateStartEndHours(startDate, endDate);
    if (startDate.hour() === endDate.hour()) {
      const minutes = endDate.minute() - startDate.minute();
      chartData[startDate.hour()].minutes += !!minutes ? minutes : 1;
    } else {
      for (let i = startHour; i <= endHour; i++) {
        if (i === startDate.hour()) {
          chartData[i].minutes += 60 - startDate.minute();
        } else if (i === endDate.hour()) {
          chartData[i].minutes += endDate.minute();
        } else {
          chartData[i].minutes = 60;
        }
      }
    }
  });
  return chartData;
}

function calculateStartEndHours(startDate, endDate) {
  let hours = {};
  if (startDate.format(DATE_FORMAT) === endDate.format(DATE_FORMAT)) {
    hours.startHour = startDate.hour();
    hours.endHour = endDate.hour();
  } else if (startDate.format(DATE_FORMAT) !== CURRENT_DATE_FILTER) {
    hours.startHour = 0;
    hours.endHour = endDate.hour();
  } else {
    hours.startHour = startDate.hour();
    hours.endHour = 23;
  }
  return hours;
}
