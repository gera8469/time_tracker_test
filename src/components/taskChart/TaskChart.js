import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import generateChartData from './utils/chartUtil';

export default function TaskChart({taskList}) {
    return (
        <ResponsiveContainer width="100%" height={400}>
          <BarChart data={generateChartData(taskList)}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}} >
            <XAxis dataKey="name"/>
            <YAxis domain={[0, 60]}/>
            <CartesianGrid strokeDasharray="3 3"/>
            <Tooltip/>
            <Legend />
            <Bar dataKey="minutes" fill="#8884d8" barSize={20}/>
          </BarChart>
        </ResponsiveContainer>
    );
}
