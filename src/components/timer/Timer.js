import React, {Component} from 'react';
import {
  RaisedButton,
  Paper,
} from 'material-ui';
import './stylesheet.css';

export default class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timerValue: this.props.isActive
          ? this.props.activeTask.getTimePassed()
          : `00:00:00`,
    };
    this.intervalId = 0;
  }

  componentDidMount() {
    if (this.props.isActive) {
      this.startTimer();
    }
  };

  componentWillUnmount() {
    this.clearTaskTimerInterval();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isActive) {
      if (!this.intervalId) {
        this.startTimer();
      }
    } else {
      this.clearTaskTimerInterval();
      this.setState({timerValue: `00:00:00`});
    }
  }

  startTimer = () => {
    this.intervalId = setInterval(() => {
      this.setState(() => ({
        timerValue:
            this.props.activeTask.getTimePassed(),
      }));
    }, 1000);
  };

  clearTaskTimerInterval = () => {
    clearInterval(this.intervalId);
    this.intervalId = 0;
  };

  render() {
    const {
      isActive,
      onClick,
    } = this.props;
    const {
      timerValue,
    } = this.state;
    return (
        <div className={'timer'}>
          <Paper
              className={'timer__counter'}
              children={timerValue}
              zDepth={3}
              circle={true}
          />
          <RaisedButton
              onClick={onClick}
              className={'timer__button'}
          >
            {isActive ? `STOP` : `START`}
          </RaisedButton>
        </div>
    );
  }
}
