import React, {Component} from 'react';
import {
  TextField,
  Dialog,
  FlatButton,
} from 'material-ui';
import {Timer} from '../';

export default class TaskManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openAlert: false,
      taskName: this.props.activeTask.taskName,
    };
  }

  onChangeTaskNameInput = event => {
    this.setState({taskName: event.target.value});
  };

  startNewTask = () => {
    this.props.onClickStart(this.state.taskName);
  };

  stopNewTask = () => {
    if (!this.state.taskName) {
      this.setState({openAlert: true});
    } else {
      this.props.activeTask.taskName = this.state.taskName;
      this.setState({taskName: ''});
      this.props.onClickStop(this.props.activeTask);
    }
  };

  handleClose = () => {
    this.setState({openAlert: false});
  };

  render() {
    const actions = [
      <FlatButton
          label="CLOSE"
          primary={true}
          onClick={this.handleClose}
      />,
    ];
    const {
      activeTask,
      isActive,
    } = this.props;

    return (
        <div className={'taskManager'}>
          <TextField
              className={'taskManager__input'}
              floatingLabelText={`Name of your task`}
              floatingLabelFixed={true}
              floatingLabelFocusStyle={{color: '#3249c6', fontSize: 30}}
              underlineFocusStyle={{borderColor: '#3249c6'}}
              onChange={this.onChangeTaskNameInput}
              value={this.state.taskName}
          />
          <Timer
              onClick={isActive ? this.stopNewTask : this.startNewTask}
              isActive={isActive}
              activeTask={activeTask}
          />
          <Dialog
              title={`Empty task name`}
              actions={actions}
              modal={false}
              open={this.state.openAlert}
              onRequestClose={this.handleClose}
          >
            You are trying close your task without name, enter the name
            and try again!
          </Dialog>
        </div>
    );
  }
}
