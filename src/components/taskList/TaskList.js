import React from 'react';
import {
  Link,
} from 'react-router-dom';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import {RaisedButton} from 'material-ui';

export default function TaskList({
                                   taskObjList,
                                   removeTaskHandler,
                                 }) {
  const taskList = taskObjList.map((task, index) => {
    return (
        <TableRow key={task.id} className={`taskTable__row`}>
          <TableRowColumn
              className={'taskTable__cell taskTable__cell_bodyCell'}>
            {index + 1}
          </TableRowColumn>
          <TableRowColumn
              className={'taskTable__cell taskTable__cell_bodyCell'}>
            {task.taskName}
          </TableRowColumn>
          <TableRowColumn
              className={'taskTable__cell taskTable__cell_bodyCell'}>
            {task.startTime}
          </TableRowColumn>
          <TableRowColumn
              className={'taskTable__cell taskTable__cell_bodyCell'}>
            {task.endTime}
          </TableRowColumn>
          <TableRowColumn
              className={'taskTable__cell taskTable__cell_bodyCell'}>
            {task.timeCompletedTask}
          </TableRowColumn>
          <TableRowColumn
              className={'taskTable__cell'}>
            <RaisedButton className={'taskTable__button taskTable__button_link'}
                containerElement={<Link to={`/info/${task.id}`}/>}>
              INFO
            </RaisedButton>
          </TableRowColumn>
          <TableRowColumn className={'taskTable__cell'}>
            <RaisedButton
                className={'taskTable__button button'}
                onClick={() => removeTaskHandler(task.id)}
            >
              DELETE
            </RaisedButton>
          </TableRowColumn>
        </TableRow>
    );
  });
  return (
      <Table className={'taskTable'}>
        <TableHeader
            displaySelectAll={false}
            adjustForCheckbox={false}
        >
          <TableRow>
            <TableHeaderColumn
                className={'taskTable__cell'}>№</TableHeaderColumn>
            <TableHeaderColumn
                className={'taskTable__cell'}>Name of tasks</TableHeaderColumn>
            <TableHeaderColumn
                className={'taskTable__cell'}>Time start</TableHeaderColumn>
            <TableHeaderColumn
                className={'taskTable__cell'}>Time end</TableHeaderColumn>
            <TableHeaderColumn
                className={'taskTable__cell'}>Time spend</TableHeaderColumn>
            <TableHeaderColumn
                className={'taskTable__cell'}>Info</TableHeaderColumn>
            <TableHeaderColumn
                className={'taskTable__cell'}>Delete</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody
            displayRowCheckbox={false}
            showRowHover={true}
        >
          {taskList}
        </TableBody>
      </Table>
  );
}
