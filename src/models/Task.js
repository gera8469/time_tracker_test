import moment from 'moment';
import {CURRENT_DATE_FILTER, DATE_FORMAT} from './TaskDataManager';

const DATE_TIME_FORMAT = `H:mm:ss DD/MM/YY`;

export default class Task {
  constructor(taskName, startDate, endDate = null) {
    this._taskState = {
      id: null,
      taskName: taskName ? taskName : '',
      startDate,
      endDate,
    };
  }

  get id() {
    return this._taskState.id;
  }

  set id(value) {
    this._taskState.id = value;
  }

  get taskName() {
    return this._taskState.taskName;
  }

  set taskName(value) {
    return this._taskState.taskName = value;
  }

  get startTime() {
    return this._timeStampToDate(this._taskState.startDate);
  }

  get endTime() {
    return this._taskState.endDate
        ? this._timeStampToDate(this._taskState.endDate) : '';
  }

  set endTimeStamp(value) {
    this._taskState.endDate = value;
  }

  get timeCompletedTask() {
    return this._taskState.endDate
        ? this.getTimePassed(this._taskState.endDate) : '';
  }

  get startTimeStamp() {
    return this._taskState.startDate;
  }

  get endTimeStamp() {
    return this._taskState.endDate;
  }

  getTimePassed(endDate = Date.now()) {
    let secs = (Math.round(endDate - this._taskState.startDate) / 1000);
    let minutes = Math.floor(secs / 60);
    secs = Math.floor(secs % 60);
    let hours = Math.floor(minutes / 60);
    minutes = minutes % 60;
    return hours + `:` + Task._fillZero(minutes) + `:` + Task._fillZero(secs);
  }

  isTaskActual(dateFilter = CURRENT_DATE_FILTER) {
    return moment(this.startTimeStamp)
            .format(DATE_FORMAT) === dateFilter
        || moment(this.endTimeStamp)
            .format(DATE_FORMAT) === dateFilter;
  }

  _timeStampToDate(timeStamp, format = DATE_TIME_FORMAT) {
    return moment(timeStamp).format(format);
  };

  static _fillZero(number) {
    return (`0` + number).slice(-2);
  };

  static createTaskByData = ({id, taskName, startDate, endDate = null}) => {
    const task = new Task(taskName, startDate, endDate);
    task.id = id;
    return task;
  }
}
