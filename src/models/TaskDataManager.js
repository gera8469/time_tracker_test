import {Task} from './';
import moment from 'moment';

export const DATE_FORMAT = `DD/MM/YY`;
export const CURRENT_DATE_FILTER = moment(Date.now()).format(DATE_FORMAT);


export class TaskDataManager {
  static addTask(task) {
    const taskData = TaskDataManager.getTaskData();
    task.id = Date.now();
    taskData.taskList[task.id] = task;
    TaskDataManager._saveTaskData(taskData);
  }

  static setActiveTask(activeTask) {
    const taskData = TaskDataManager.getTaskData();
    taskData.activeTask = activeTask;
    TaskDataManager._saveTaskData(taskData);
  }

  static getActiveTaskObj() {
    const activeTaskData = TaskDataManager.getTaskData().activeTask;
    return activeTaskData
        ? Task.createTaskByData(activeTaskData._taskState) : null;
  }

  static clearActiveTask() {
    const taskData = TaskDataManager.getTaskData();
    taskData.activeTask = null;
    TaskDataManager._saveTaskData(taskData);
  }

  static removeTask(taskId) {
    const taskData = TaskDataManager.getTaskData();
    const activeTask = TaskDataManager.getActiveTaskObj();
    if (activeTask && activeTask.id === taskId) {
      taskData.activeTask = null;
    }
    if (taskData.taskList[taskId]) {
      delete taskData.taskList[taskId];
    }
    TaskDataManager._saveTaskData(taskData);
    return TaskDataManager._generateTaskObjList(taskData.taskList);
  }

  static getTaskById(taskId) {
    const allTaskData = TaskDataManager.getTaskData();
    const taskData = allTaskData.taskList[taskId];
    return taskData ? Task.createTaskByData(taskData._taskState) : '';
  }

  static getTaskObjList() {
    const taskListData = TaskDataManager.getTaskData().taskList;

    return Object.keys(taskListData).map(taskId => {
      return Task.createTaskByData(taskListData[taskId]._taskState);
    });
  }

  static _generateTaskObjList(taskListData = []) {
    return Object.keys(taskListData).map(taskId => {
      return Task.createTaskByData(taskListData[taskId]._taskState);
    });
  }

  static getTaskData() {
    let jsonTaskData = window.localStorage.getItem('taskData');
    let taskData;
    if (!jsonTaskData) {
      taskData = {
        activeTask: null,
        taskList: {},
      };
    } else {
      taskData = JSON.parse(jsonTaskData);
    }
    return taskData;
  }

  static _saveTaskData(taskData) {
    window.localStorage.setItem('taskData', JSON.stringify(taskData));
  }
}
